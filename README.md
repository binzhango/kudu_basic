# kudu_basic

[Using Apache Kudu with Apache Impala](http://kudu.apache.org/docs/kudu_impala_integration.html#advanced_partitioning)

## External table
```sql
CREATE EXTERNAL TABLE my_mapping_table
STORED AS KUDU
TBLPROPERTIES(
'kudu.table_name'='my_kudu_table'
)
```
## Interal table
```sql
CREATE TABLE my_first_table1
(
  id BIGINT,
  name STRING,
  PRIMARY KEY(id)   # 使用PRIMARY KEY(id) 而不是 'kudu.key_columns' = 'id'
)PARTITION BY HASH PARTITIONS 16 STORED AS KUDU
TBLPROPERTIES (
  'kudu.table_name' = 'my_kudu_table',
  'kudu.master_addresses' = 'abc:7051'
);
	
 CREATE TABLE `my_first_table` (
`id` BIGINT,
`name` STRING
)
TBLPROPERTIES(
  'storage_handler' = 'com.cloudera.kudu.hive.KuduStorageHandler',
  'kudu.table_name' = 'my_first_table',
  'kudu.master_addresses' = 'kudu-master.example.com:7051',
  'kudu.key_columns' = 'id'
); 
```


- **storage_handler**: the mechanism used by Impala to determine the type of data source. For Kudu tables, this must be com.cloudera.kudu.hive.KuduStorageHandler.
- **kudu.table_name**: the name of the table that Impala will create (or map to) in Kudu
- **kudu.master_addresses**: the list of Kudu masters with which Impala should communicate
- **kudu.key_columns**: the comma-separated list of primary key columns, whose contents should not be nullable

**kudu.master_addresses** is essential.
if using **create table**, the first column should be **primary key**.

### create table as select
```sql
CREATE TABLE new_table
PRIMARY KEY (ts, name)
PARTITION BY HASH(name) PARTITIONS 8
STORED AS KUDU
AS SELECT ts, name, value FROM old_table;
```
**The primary should be defined in schema**


